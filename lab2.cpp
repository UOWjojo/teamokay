#include <iostream>
#include<limits>
#include<conio.h>
#include<math.h>
using namespace std;
void f1();
void f2();
void f3();
void f4();
void f5();
void f6();
void f7();
void f8();

int main()
{
	int i = 0;
	do
	{
		cout << "1. Convert Celsius to Fahrenheit" << endl;
		cout << "2. Convert Fahrenheit to Celsius" << endl;
		cout << "3. Calculate Circumference of a circle" << endl;
		cout << "4. Calculate Area of a circle" << endl;
		cout << "5. Area of Rectangle" << endl;
		cout << "6. Area of Triangle (Heron��s Formula)" << endl;
		cout << "7. Volume of Cylinder" << endl;
		cout << "8. Volume of Cone" << endl;
		cout << "9. Quit program" << endl;
		cout << "Please enter you option: ";
		cin >> i;
		while (cin.fail() || i < 1 || i > 9)
		{
			cin.clear();
			cin.ignore();
			cout << "Invalid Option! Please try again. " << endl;
			cin >> i;
		}

		switch (i)
		{
		case 1: {
			cin.clear();
			cin.ignore();
			f1();
			break;
		}
		case 2: {
			cin.clear();
			cin.ignore();
			f2();
			break;
		}
		case 3: {
			cin.clear();
			cin.ignore();
			f3();
			break;
		}
		case 4: {
			cin.clear();
			cin.ignore();
			f4();
			break;
		}
		case 5: {
			cin.clear();
			cin.ignore();
			f5();
			break;
		}
		case 6: {
			cin.clear();
			cin.ignore();
			f6();
			break;
		}
		case 7: {
			cin.clear();
			cin.ignore();
			f7();
			break;
		}
		case 8: {
			cin.clear();
			cin.ignore();
			f8();
			break;
		}
		case 9: {
			cin.clear();
			cin.ignore();
			cout << "Bye Bye" << endl;
			return 0;
		}
		default:
			cout << "Invalid Option !" << endl;
		}
	} while (option != 9);
	return 0;
}

void f1()
{
	float fah, cel;

	cout << "Convert Celsius to Fahrenheit" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the celsius : ";
	cin >> cel;
	fah = (cel*1.8) + 32;
	cout << "The celsius in fahrenheit is : " << fah << endl;
	cout << "--------------------------------------" << endl;
}

void f2()
{
	float fah, cel;
	cout << "Convert Fahrenheit to Celsius" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the fahrenheit : ";
	cin >> fah;
	cel = (fah - 32) / 1.8;
	cout << "The fahrenheit in celsius is : " << cel << endl;
	cout << "--------------------------------------" << endl;

}

void f3()
{
	const double p1 = 3.14;
	double radius;
	double diameter;
	double circumference;
	cout << "Calculate Circumference of a circle" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the radius of circle: ";
	cin >> radius;
	while ((1 > radius) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input! please try again." << endl;
		cin >> radius;
	}
	diameter = 2 * radius;
	circumference = p1 * diameter;
	cout << "C=The circumference of a circle is : " << circumference << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;
}

void f4()
{
	const double p1 = 3.14;
	double area;
	double radius;
	cout << "Calculate Area of a circle" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the radius of circle: ";
	cin >> radius;
	while ((1 > radius) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> radius;
	}
	area = p1 * (radius*radius);
	cout << "The area of circle is: " << area << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;
}

void f5()
{
	int area, l, h;
	cout << "Area of Rectangle" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the length of Rectangle: ";
	cin >> l;
	while ((1 > l) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> l;
	}
	cout << "Please enter the height of Rectangle: ";
	cin >> h;
	while ((1 > h) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> h;
	}
	area = l * h;
	cout << "The area of rectangle is: " << area << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;
}

void f6()
{
	float a, b, c, area, s;
	cout << "Area of Triangle" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the first value of triangle: ";
	cin >> a;
	while ((1 > a) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> a;
	}
	cout << "Please enter the second value of triangle: ";
	cin >> b;
	while ((1 > b) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> b;
	}
	cout << "Please enter the third value of triangle: ";
	cin >> c;
	while ((1 > c) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invalid input, please try again" << endl;
		cin >> c;
	}
	s = (a + b + c) / 2;
	area = sqrt(s*(s - a)*(s - b)*(s - c));
	cout << "The area of triangle is: " << area << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;
}

void f7()
{
	double p1 = 3.14, r, h, vol;
	cout << "Volume of Cylinder" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the height of Cylinder: ";
	cin >> h;
	while ((1 > h) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invaild input, please try again" << endl;
		cin >> h;
	}
	cout << "Please enter the radius of Cylinder: ";
	cin >> r;
	while ((1 > r) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invaild input, please try again" << endl;
		cin >> r;
	}
	vol = (p1*r*r*h);
	cout << "The volume of the cylinder is: " << vol << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;
}

void f8()
{
	double p1 = 3.14, r, h, vol;
	cout << "Volume of Cone" << endl;
	cout << "--------------------------------------" << endl;
	cout << "Please enter the height of Cone: ";
	cin >> h;
	while ((1 > h) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invaild input, please try again" << endl;
		cin >> h;
	}
	cout << "Please enter the radius of Cone: ";
	cin >> r;
	while ((1 > r) || cin.fail())
	{
		cin.clear();
		cin.ignore();
		cout << "Invaild input, please try again" << endl;
		cin >> r;
	}
	vol = (p1*r*r*h / 3);
	cout << "The volume of Cone is: " << vol << endl;
	cout << "--------------------------------------" << endl;
	cout << endl;
}
